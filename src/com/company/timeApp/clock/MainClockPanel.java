package com.company.timeApp.clock;

import com.company.timeApp.common.ClockPanel;
import com.company.timeApp.common.IconFactory;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalTime;
import java.util.Timer;
import java.util.TimerTask;

/**
 * shows the clock with the current time
 */
public class MainClockPanel extends JPanel {

    private ClockPanel clockPanel;
    private JLabel jLabel;
    private Timer timer;


    public MainClockPanel() {
        setLayout(new BorderLayout());
        clockPanel = new ClockPanel(false);
        jLabel = new JLabel();
        jLabel.setFont(new IconFactory("assets/clcok_font.ttf",30).getFont());
        jLabel.setHorizontalAlignment(SwingConstants.CENTER);
        add(clockPanel, BorderLayout.CENTER);
        add(jLabel, BorderLayout.SOUTH);
        resumeClock();
    }


    /**
     * starts or resumes clock timer
     */
    public void resumeClock() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                clockPanel.recalculate(LocalTime.now());
                jLabel.setText(clockPanel.getCurrentTime());
            }
        }, 0, 1000);
    }

    /**
     * stops the timer
     */
    public void stopClock() {
        timer.purge();
        timer.cancel();
    }

    public static Font getFont(String path) {
        try (InputStream in = MainClockPanel.class.getClassLoader().getResourceAsStream(path)) {
            return Font.createFont(Font.TRUETYPE_FONT, in);
        } catch (FontFormatException | IOException | NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }
}
