package com.company.timeApp;


import com.company.timeApp.stopwatch.StopWatchPanel;
import com.company.timeApp.clock.MainClockPanel;

import javax.swing.*;
import java.awt.*;

public class TimeApp extends JFrame {


    private JTabbedPane jTabbedPane;
    private Rectangle oldBounds;
    private MainClockPanel worldPanel;
    private StopWatchPanel stopWatchPanel;


    public TimeApp() throws HeadlessException {

        setMinimumSize(new Dimension(600, 600));
        setLayout(new GridLayout(1, 1));
        jTabbedPane = new JTabbedPane(JTabbedPane.BOTTOM, JTabbedPane.WRAP_TAB_LAYOUT);
        worldPanel =new MainClockPanel();
        stopWatchPanel = new StopWatchPanel();
        jTabbedPane.addTab("Clock", worldPanel);
        jTabbedPane.addTab("Stopwatch", stopWatchPanel);
//        jTabbedPane.addTab("test3", new JPanel());

        jTabbedPane.addChangeListener(e -> {
            if (jTabbedPane.getSelectedIndex() == 0) {
                worldPanel.resumeClock();
            } else {
                worldPanel.stopClock();

            }
        });

        add(jTabbedPane);



        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        /*addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                Rectangle b = e.getComponent().getBounds();

                System.out.println(b.getBounds().toString());
                float ratio = 16f / 9f;



                if ((b.width * ratio) > screenSize.height)
                    b.width = (int) (screenSize.height / ratio);

                e.getComponent().setBounds(b.x, b.y, b.width, (int) (b.width * ratio));
                invalidate();
                repaint();
            }
        });*/

    }
}
