package com.company.timeApp.stopwatch;

import com.company.timeApp.common.IconFactory;
import com.company.timeApp.layout.TransparentListCellRenderer;
import mdlaf.animation.MaterialUIMovement;
import mdlaf.utils.MaterialColors;
import org.jdesktop.swingx.painter.AbstractLayoutPainter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import static com.company.timeApp.stopwatch.RightStopWatchPanel.StopWatchState.*;
import static java.awt.GridBagConstraints.*;


public class RightStopWatchPanel extends JPanel implements ActionListener {

    private JLabel currentTime;
    private JPanel actionsPanel;
    private JScrollPane scrollPane;
    private JButton startBtn, resetBtn, lapBtn;
    private JList<String> lapsList;
    private StopWatchClickListener stopWatchClickListener;
    private StopWatchState stopWatchState = STOPPED;
    private DefaultListModel<String> laps;

    public RightStopWatchPanel(StopWatchClickListener stopWatchClickListener) {
        setLayout(new GridBagLayout());
        this.stopWatchClickListener = stopWatchClickListener;
        laps = new DefaultListModel<>();

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        currentTime = new JLabel("",SwingConstants.CENTER);
        currentTime.setFont(new IconFactory("assets/clcok_font.ttf", 40).getFont());


        gridBagConstraints.fill = HORIZONTAL;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipady = 50;
        add(currentTime, gridBagConstraints);

        resetBtn = new JButton("reset");
        gridBagConstraints.fill = HORIZONTAL;
        resetBtn.setVisible(false);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.insets.right=10;
        gridBagConstraints.insets.left=10;
        gridBagConstraints.ipady = 10;
        add(resetBtn, gridBagConstraints);

        startBtn = new JButton("start");
        startBtn.setVisible(true);

        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        add(startBtn, gridBagConstraints);

        lapBtn = new JButton("lap");
        lapBtn.setVisible(false);
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        add(lapBtn, gridBagConstraints);


        resetBtn.addActionListener(this);
        startBtn.addActionListener(this);
        lapBtn.addActionListener(this);

        lapsList = new JList<>();
        lapsList.setCellRenderer(new TransparentListCellRenderer());
        lapsList.setValueIsAdjusting(true);
        scrollPane = new JScrollPane();
        gridBagConstraints.fill = BOTH;
        scrollPane.setViewportView(lapsList);
        scrollPane.setVisible(false);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        add(scrollPane, gridBagConstraints);

    }


    public void setCurrentTime(String time) {
        currentTime.setText(time);
//        Utils.setFontSize(currentTime);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == resetBtn) {
            resetBtn.setVisible(false);
            lapBtn.setVisible(false);
            startBtn.setText("start");
            stopWatchClickListener.onResetClick();
            setCurrentTime("0.00");
            stopWatchState = STOPPED;
            laps.clear();
            scrollPane.setVisible(false);
            return;
        }
        if (e.getSource() == startBtn) {
            resetBtn.setVisible(true);
            lapBtn.setVisible(true);
            switch (stopWatchState) {
                case STOPPED:
                    stopWatchState = STARTED;
                    break;
                case PAUSED:
                    stopWatchState = RESUMED;
                    break;
                case STARTED:
                case RESUMED:
                    stopWatchState = PAUSED;
                    break;
            }
            startBtn.setText(stopWatchState == STARTED || stopWatchState == RESUMED ? "pause" : "start");
            stopWatchClickListener.onStartStopClick(stopWatchState);
            return;
        }
        if (e.getSource() == lapBtn) {

            if (stopWatchState != PAUSED) {

                scrollPane.setVisible(true);
                laps.add(0, currentTime.getText());


                lapsList.setModel(laps);
            }
        }
    }


    public interface StopWatchClickListener {
        public void onResetClick();

        public void onStartStopClick(StopWatchState stopWatchState);
    }

    /**
     * Defines the state of the stopwatch
     */
    public enum StopWatchState {
        STOPPED, STARTED, PAUSED, RESUMED
    }

}

