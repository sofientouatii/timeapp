package com.company.timeApp.stopwatch;

import com.company.timeApp.common.ClockPanel;
import com.company.timeApp.stopwatch.RightStopWatchPanel.StopWatchState;

import javax.swing.*;
import java.awt.*;
import java.time.LocalTime;
import java.util.*;
import java.util.Timer;

import static com.company.timeApp.stopwatch.RightStopWatchPanel.StopWatchState.*;

/**
 * Main StopWatch Panel
 */
public class StopWatchPanel extends JPanel implements RightStopWatchPanel.StopWatchClickListener {
    Timer analog, digital;
    private ClockPanel clockPanel;
    private RightStopWatchPanel rightStopWatchPanel;
    private LocalTime interruptTime, pausedTime, startTime;


    public StopWatchPanel() {

        setLayout(new GridLayout(1, 2));
        clockPanel = new ClockPanel(true);
        rightStopWatchPanel = new RightStopWatchPanel(this);
        add(clockPanel);
        add(rightStopWatchPanel);
        LocalTime localTime = LocalTime.MIN;
        clockPanel.recalculate(localTime);
        rightStopWatchPanel.setCurrentTime("0.00");

    }

    @Override
    public void onResetClick() {
        analog.cancel();
        analog.purge();
        digital.purge();
        digital.cancel();
        pausedTime=null;
        interruptTime=null;
        clockPanel.recalculate(LocalTime.MIN);
    }

    @Override
    public void onStartStopClick(StopWatchState stopWatchState) {
        switch (stopWatchState) {
            case STARTED:

                startTime = LocalTime.now();
                runClock(stopWatchState);
                break;
            case RESUMED:
                if (pausedTime != null) {
                    pausedTime = LocalTime.now().minusNanos(interruptTime.toNanoOfDay()).plusNanos(pausedTime.toNanoOfDay());
                } else
                    pausedTime = LocalTime.now().minusNanos(interruptTime.toNanoOfDay());
                runClock(stopWatchState);
                break;
            case PAUSED:

                interruptTime = LocalTime.now();

                analog.cancel();
                digital.cancel();
                break;

        }
    }


    /**
     * @param stopWatchState : {@link StopWatchState}
     *  starts then analog and digital timer
     */
    void runClock(StopWatchState stopWatchState) {
        analog = new Timer();
        digital = new Timer();
        analog.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                LocalTime time = LocalTime.now().minusNanos(startTime.toNanoOfDay());
                if (stopWatchState == RESUMED)
                    time = time.minusNanos(pausedTime.toNanoOfDay());
                clockPanel.recalculate(time);
            }
        }, 0, 1000);

        digital.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                LocalTime time = LocalTime.now().minusNanos(startTime.toNanoOfDay());
                if (stopWatchState == RESUMED)
                    time = time.minusNanos(pausedTime.toNanoOfDay());
                rightStopWatchPanel.setCurrentTime(clockPanel.setCurrentTime(time, false));
            }
        }, 0, 10);
    }
}
