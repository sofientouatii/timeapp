package com.company.timeApp.common;

import mdlaf.utils.MaterialColors;

import javax.swing.*;
import java.awt.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public class ClockPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private int xs, ys, xm, ym, xh, yh;
    private Graphics2D g2d;
    private String currentTime;
    private boolean isStopWatch;



    public ClockPanel(Boolean isStopWatch) {
        this.setDoubleBuffered(true);
        this.isStopWatch = isStopWatch;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        removeAll();
        g2d = (Graphics2D) g;
        g2d.setBackground(Color.black);
        g2d.translate(getWidth() / 2, getHeight() / 2);
        int side = getWidth() > getHeight()
                ? getHeight() : getWidth();
        g2d.setBackground(Color.black);
        g2d.scale(side / 250f, side / 250f);
        setAllRenderingHints();
        drawClockFace();
        drawClockHands();
    }

    public void recalculate(LocalTime time) {
        int seconds = time.getSecond();
        xs = (int) (Math.cos(seconds * Math.PI / 30 -
                Math.PI / 2) * 90 + 0);
        ys = (int) (Math.sin(seconds * Math.PI / 30 -
                Math.PI / 2) * 90 + 0);

        int minutes = time.getMinute();
        xm = (int) (Math.cos(minutes * Math.PI / 30 -
                Math.PI / 2) * 70 + 0);
        ym = (int) (Math.sin(minutes * Math.PI / 30 -
                Math.PI / 2) * 70 + 0);

        int hours = time.getHour();

        double a = (hours * 30 + minutes / 2f) * Math.PI / 180 - Math.PI / 2;

        xh = (int) (Math.cos(a) * 50 + 0);
        yh = (int) (Math.sin(a) * 50 + 0);
        setCurrentTime(time, true);
        repaint();
    }

    private void setAllRenderingHints() {
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING,
                RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_LCD_CONTRAST, 100);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
                RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
                RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
                RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL,
                RenderingHints.VALUE_STROKE_PURE);

    }

    private void drawClockFace() {

        for (int i = 0; i < 60; i++) {
            if ((i % 5) == 0) {
                g2d.setColor(new Color(255, 255, 255, 50));
                g2d.setStroke(new BasicStroke(2.0f));
                g2d.drawLine(88, 0, 96, 0);

            } else if (isStopWatch) {
                g2d.setStroke(new BasicStroke(1.0f));
                g2d.setColor(new Color(255, 255, 255, 60));
                g2d.drawLine(92, 0, 96, 0);
            }
            g2d.rotate((Math.PI / 180.0) * 6.0);
        }

    }

    /**
     * draws the clock hands
     */
    private void drawClockHands() {
        g2d.setColor(MaterialColors.LIME_500);
        g2d.setStroke(new BasicStroke(4.0f));
        g2d.drawLine(0, 0, xh, yh);
        g2d.setStroke(new BasicStroke(2.0f));
        g2d.drawLine(0, 0, xm, ym);
        g2d.setColor(MaterialColors.GRAY_500);
        g2d.setStroke(new BasicStroke(1f));
        g2d.drawLine(0, 0, xs, ys);
        g2d.setColor(Color.white);
        g2d.fillOval(-5, -5, 10, 10);
        g2d.setColor(MaterialColors.LIME_500);
        g2d.setStroke(new BasicStroke(1));
        g2d.fillOval(-2, -2, 4, 4);
    }


    /**
     * @param time                :{@link LocalTime} time on the clock/stopwatch
     * @param shouldUpdate:{@link : boolean} when set true the @currentTime will be Updated
     * @return @{@link String}: formatted time
     */
    public String setCurrentTime(LocalTime time, boolean shouldUpdate) {
        DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();

        if (!isStopWatch)
            builder.appendPattern("HH:mm:ss");
        else {
            if (time.getHour() > 0)
                builder.appendPattern("H:");
            if (time.getMinute() > 0) {
                if (time.getHour() == 0)
                    builder.appendPattern("m:");
                else
                    builder.appendPattern("mm:");
            }
            if (time.getHour() == 0 && time.getMinute() == 0)
                builder.appendPattern("s.");
            else
                builder.appendPattern("ss.");
            builder.appendFraction(ChronoField.MILLI_OF_SECOND, 2, 2, false);
        }

        DateTimeFormatter formatter = builder.toFormatter();

        if (shouldUpdate) this.currentTime = formatter.format(time);
        return time.format(formatter);

    }


    /**
     * @return @currentTime
     */
    public String getCurrentTime() {
        return currentTime;
    }

}
