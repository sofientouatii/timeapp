package com.company.timeApp.common;


import java.awt.*;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Objects;

public class IconFactory {
    /**
     * Creates the icon factory
     */

    private Font font;

    public Font getFont() {
        return font;
    }

    public void setFont(String fontPath, float deriveFont) {
        try {
            InputStream stream = new BufferedInputStream(new FileInputStream(fontPath));
            font = Font.createFont(Font.TRUETYPE_FONT, stream);

            font = font.deriveFont(deriveFont);


        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public IconFactory(String fontPath, int deriveFont) {
        setFont(fontPath, deriveFont);
    }
}
